import React, { useState, useEffect, useRef } from 'react';
import { Div, ThemeProvider, Button, Icon } from 'atomize';
import { useDispatch } from 'react-redux';
import { setFooterInView } from '../../Stores/pagestore';
import { elementInViewport2 } from '../../helpers/divelements';
import Link from 'next/link';
import { useRouter } from 'next/router';

const theme = {
	grid: {
		colCount: 10,
		gutterWidth: 0,
	},
	textSize: {
		size: {
			verySmallSize: '10px',
		},
	},
};

export default function Footer() {
	const dispatch = useDispatch();
	const router = useRouter();
	const _ref = useRef();
	const [state, setState] = useState({ notVisible: false });
	const handleSetFooterInView = (footerInView) => {
		dispatch(setFooterInView({ footerInView }));
	};
	const footerScrollListener = () => {
		if (_ref.current) {
			elementInViewport2(_ref.current, handleSetFooterInView.bind(this));
		}
	};
	useEffect(() => {
		if (!!router) {
			setState({
				...state,
				notVisible: router.pathname === '/marketplace/fitprofile',
			});
		}
	}, [router]);
	useEffect(() => {
		window.addEventListener('scroll', footerScrollListener.bind(this));
		return () => {
			window.removeEventListener(
				'scroll',
				footerScrollListener.bind(this),
			);
		};
	}, []);
	return state.notVisible ? (
		<></>
	) : (
		<ThemeProvider theme={theme}>
			<Div
				ref={_ref}
				className='footer-main'
				w='100%'
				bg='#1a89f0'
				p={{ x: '2rem', y: '1rem' }}
				minH='fit-content'
				textColor='white'>
				<Div className='footer-row'>
					<Div className='truklay-white-logo truklay-footer-size' />
					<Div className='footer-desktop' flexGrow='1' />
				</Div>
				<Div className='footer-row footer-desktop' p={{ x: '4px' }}>
					<Div className='footer-column'>
						<Div className='footer-cell'>
							<a>CUSTOMER SERVICE</a>
						</Div>
						<Div className='footer-cell'>
							<a>SHIPPING INFORMATION</a>
						</Div>
						<Div className='footer-cell'>
							<a>RETURNS</a>
						</Div>
					</Div>
					<Div className='footer-column'>
						<Div className='footer-cell'>
							<a>TRACK ORDERS</a>
						</Div>
						<Link href='/legaldocs?docs=TERMS+CONDITIONS'>
							<Div className='footer-cell'>
								<a>TERMS & CONDITIONS</a>
							</Div>
						</Link>
						<Link href='/legaldocs?docs=PRIVACY+POLICY'>
							<Div className='footer-cell'>
								<a>PRIVACY POLICY</a>
							</Div>
						</Link>
					</Div>
					<Div className='footer-column'>
						<Div className='footer-cell'>
							<a>ABOUT TEKNOFEET</a>
						</Div>
						<Div d='flex'>
							<Div className='facebook-logo footer-icon'></Div>
							<Div className='instagram-logo footer-icon'></Div>
							<Div className='linkedIn-logo footer-icon'></Div>
						</Div>
						<Div flexGrow='1' />
					</Div>
					<Div className='footer-column'>
						<Div
							className='footer-cell '
							style={{ marginBottom: '0px' }}>
							<a>YOUR VOICE MATTERS</a>
						</Div>
						<Div className='footer-cell footer-subcontent'>
							We’d love to learn more about your <br />
							shopping experiences on teknofeet.com <br />
							and how we can improve!
						</Div>
						<Div className='footer-cell footer-take-survey'>
							<Button
								textSize='14px'
								h='fit-content'
								p={{ l: '0.8rem', r: '1rem', y: '0.5rem' }}
								bg='transparent'
								hoverBg='white'
								hoverTextColor='#58606a'
								border='1px solid'
								borderColor='white'
								rounded='circle'
								hoverShadow='3'
								m={{ b: '1rem' }}
								prefix={
									<Icon
										name='Edit'
										size='20px'
										color='white'
									/>
								}>
								Take Survey
							</Button>
						</Div>
					</Div>
				</Div>
				<Div className='footer-row footer-mobile' p={{ x: '4px' }}>
					<Div className='footer-column'>
						<Div className='footer-cell'>
							<a>ABOUT TEKNOFEET</a>
						</Div>
						<Div className='footer-cell'>
							<a>CUSTOMER SERVICE</a>
						</Div>
						<Div className='footer-cell'>
							<a>SHIPPING INFORMATION</a>
						</Div>
						<Div className='footer-cell'>
							<a>RETURNS</a>
						</Div>
						<Div className='footer-cell'>
							<a>TRACK ORDERS</a>
						</Div>
						<Link href='/legaldocs?docs=TERMS+CONDITIONS'>
							<Div className='footer-cell'>
								<a>TERMS & CONDITIONS</a>
							</Div>
						</Link>
						<Link href='/legaldocs?docs=PRIVACY+POLICY'>
							<Div className='footer-cell'>
								<a>PRIVACY POLICY</a>
							</Div>
						</Link>
					</Div>
					<Div className='footer-column'>
						<Div
							className='footer-cell '
							style={{ marginBottom: '0px' }}>
							<a>YOUR VOICE MATTERS</a>
						</Div>
						<Div className='footer-cell footer-subcontent'>
							We’d love to learn more about your <br />
							shopping experiences on teknofeet.com <br />
							and how we can improve!
						</Div>
						<Div className='footer-cell footer-take-survey'>
							<Button
								textSize='14px'
								h='fit-content'
								p={{ l: '0.8rem', r: '1rem', y: '0.5rem' }}
								bg='transparent'
								hoverBg='white'
								hoverTextColor='#58606a'
								border='1px solid'
								borderColor='white'
								rounded='circle'
								hoverShadow='3'
								m={{ b: '1rem' }}
								prefix={
									<Icon
										name='Edit'
										size='20px'
										color='white'
									/>
								}>
								Take Survey
							</Button>
						</Div>
						<Div d='flex'>
							<Div className='facebook-logo footer-icon'></Div>
							<Div className='instagram-logo footer-icon'></Div>
							<Div className='linkedIn-logo footer-icon'></Div>
						</Div>
					</Div>
					{/* <Div className='footer-column'>
						<Div flexGrow='1' />
					</Div> */}
				</Div>
				<Div className='footer-row column-flex' p={{ x: '4px' }}>
					<Div className='footer-column' w='100%'>
						<Div style={{ height: '1rem' }} />
						<Div className='line-divider' />
						<Div className='footer-cell footer-copyright'>
							Copyright © 2020 - Tecknofeet.com
						</Div>
					</Div>
				</Div>
			</Div>
		</ThemeProvider>
	);
}
