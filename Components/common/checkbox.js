import { Div, Icon, Text } from 'atomize';

export default function Checkbox({ checked, handleChange, label }) {
	return (
		<Div
			onClick={() => {
				handleChange();
			}}
			className='custom-checkbox'>
			<Div
				className={
					checked
						? 'custom-checkbox-icon active-checkbox'
						: 'custom-checkbox-icon'
				}
				bg={checked ? '#1a89f0' : 'white'}>
				<Icon
					name='Checked'
					size='16px'
					color={checked ? 'white' : 'transparent'}
				/>
			</Div>
			{label && (
				<Div className='checkbox-label' w='100%'>
					{label}
				</Div>
			)}
		</Div>
	);
}
