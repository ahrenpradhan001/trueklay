import React, { useState } from 'react';
import ReactPaginate from 'react-paginate';

const PaginationPanel = ({ totalPage, handlePageClick}) => {
	const pageCount = 5;
	const [state, setState] = useState({
		selected: 0,
	});
	const handleClick = (data) => {
		// let selected = data.selected;
		window.scrollTo({
			top: 0,
			behavior: 'smooth',
		});
		setState({ ...state, ...data });
		handlePageClick(data.selected)
	};
	return (
		<>
			<ReactPaginate
				previousLabel={'<'}
				nextLabel={'>'}
				breakLabel={'...'}
				breakClassName={'break-me'}
				pageCount={totalPage}
				marginPagesDisplayed={2}
				pageRangeDisplayed={5}
				onPageChange={(handleClick)}
				containerClassName={'pagination'}
				subContainerClassName={'pages pagination'}
				activeClassName={'active'}
			/>
		</>
	);
};

export default PaginationPanel;