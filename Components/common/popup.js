import React, { useState, useEffect } from 'react';
import { Div, Button, Modal, Icon, Text } from 'atomize';

const CentralModal = ({
	isOpen,
	onClose,
	popupHeader,
	popupContent,
	popupFooter,
	popupType,
}) => {
	return (
		<Modal
			isOpen={isOpen}
			onClose={() => {
				if (onClose) {
					onClose();
				}
			}}
			className={`modal-popup ${popupType || ''}`}>
			{onClose != null && (
				<>
					<Div
						className='modal-back-button'
						pos='absolute'
						left='0'
						top='0'
						m='2em'
						style={{ zIndex: '1' }}
						onClick={onClose}>
						{/* <Button
							onClick={onClose}
							bg='#f8f8f8'
							h='fit-content'
							w='fit-content'
							p='0'
							rounded='circle'>
						</Button> */}
						<Icon name='LeftArrow' size='24px' />
					</Div>
					<Div
						className='modal-close-button'
						pos='absolute'
						right='0'
						top='0'
						m='2em'
						style={{ zIndex: '1' }}>
						<Button
							onClick={onClose}
							bg='#f8f8f8'
							h='fit-content'
							w='fit-content'
							p='0'
							border='2px solid'
							borderColor='#707070'
							rounded='circle'>
							<Icon name='Cross' color='#3e3e3e' size='20px' />
						</Button>
					</Div>
				</>
			)}
			<Div
				d='flex'
				flexDir='column'
				w='100%'
				className='modal-popup2'
				pos='relative'>
				<Div className='modal-popup-header'>{popupHeader}</Div>
				<Div className='modal-popup-content'>{popupContent}</Div>
				<Div className='modal-popup-footer'>{popupFooter}</Div>
			</Div>
		</Modal>
	);
};

const Popup = ({
	open,
	popupHeader,
	popupContent,
	popupFooter,
	onClose,
	popupType,
}) => {
	return (
		<CentralModal
			isOpen={open}
			popupHeader={popupHeader}
			popupContent={popupContent}
			popupFooter={popupFooter}
			onClose={onClose}
			popupType={popupType}
		/>
	);
};
export default Popup;
