import { useEffect } from 'react';
import { Div, Image } from 'atomize';
import { useSelector } from 'react-redux';
import { useRouter } from 'next/router';

const LoadingScreen = () => {
	const pageStore = useSelector((state) => state.pageStore);
	const shopifyStore = useSelector((state) => state.shopifyStore);
	const router = useRouter();
	let loadingTimer = null;
	useEffect(() => {
		clearTimeout(loadingTimer);
		loadingTimer = setTimeout(() => {
			if (pageStore?.loading && !!router) {
				router.push(router.asPath);
			}
		}, 8000);
	}, [router]);
	useEffect(() => {
		if (pageStore?.loading) {
			clearTimeout(loadingTimer);
		}
	}, [pageStore]);
	return !!router && pageStore?.loading ? (
		<Div className='loading-screen'>
			<Div
				className={
					router.pathname.includes('fitprofile') ||
					shopifyStore?.client?.keepMarketplaceSplashScreen
						? 'loading-screen-content loading-screen-content-full-screen'
						: 'loading-screen-content'
				}>
				<Image
					src='https://cdn.shopify.com/s/files/1/0550/2120/7721/files/loader.png?v=1614925693'
					className='vegas-spin'
				/>
			</Div>
		</Div>
	) : (
		<></>
	);
};

export default LoadingScreen;
