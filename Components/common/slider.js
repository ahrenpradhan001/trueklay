import React, { useState, useEffect, useRef } from 'react';
import { Div } from 'atomize';
import { Player } from 'video-react';
import Flickity from 'react-flickity-component';
import dynamic from 'next/dynamic';

const asNavfor = dynamic(() => import('flickity-as-nav-for'), { ssr: false });

const flickityMain = {
	pageDots: false,
	contain: true,
	draggable: false,
	prevNextButtons: false,
	freeScroll: true,
	freeScrollFriction: 0.03,
};
const flickityMain_mobile = {
	pageDots: true,
	contain: true,
	draggable: true,
	prevNextButtons: true,
	freeScroll: false,
	freeScrollFriction: 0.03,
};
const flickityMain_CarouselSlider = {
	pageDots: false,
	draggable: true,
	initialIndex: 1,
	prevNextButtons: true,
	freeScroll: false,
	freeScrollFriction: 0.03,
};

const flickityNav = {
	asNavFor: '.carousel.carousel-main',
	pageDots: false,
	contain: true,
};

const Slider = ({ images, mobileMode }) => {
	const SliderMain = useRef();
	const SliderNav = useRef();
	const handleThumbnailClick = (index) => {
		if (!mobileMode) {
			SliderNav.current.select(index);
		}
		SliderMain.current.select(index);
	};
	useEffect(() => {
		if (!mobileMode) {
			SliderNav.current.on('change', function (index) {
				SliderMain.current.select(index);
				// setCurrentSelected(index);
			});
			SliderMain.current.on('change', function (index) {
				SliderNav.current.select(index);
				// setCurrentSelected(index);
			});
		}
	}, []);
	return (
		<Div h='100%'>
			{asNavfor && (
				<>
					<div className='carousel-gallery-wrapper'>
						<Flickity
							className={'carousel-gallery'} // default ''
							flickityRef={(c) => (SliderMain.current = c)}
							options={mobileMode ? flickityMain_mobile : flickityMain} // takes flickity options {}
							// reloadOnUpdate // default false
							// static // default false
						>
							{images.map((i, index) =>
								i.type == 'video' ? (
									<div
										style={{
											display: 'flex',
											flexDirection: 'column',
											justifyContent: 'center',
											width: '100%',
											height: '100%',
										}}>
										<Player
											playsInline
											poster={i.thumbnail}
											// src='https://media.w3.org/2010/05/sintel/trailer_hd.mp4'
											src={i.video}
										/>
									</div>
								) : (
									<Div
										key={index}
										borderColor='#fff'
										bgImg={i.original}
										// rounded='1em'
										bgSize='cover'
										bgPos='center'
										h='100%'
										w='100%'
									/>
								),
							)}
						</Flickity>
					</div>
					{!mobileMode && (
						<Flickity
							className={'carousel-gallery-nav'} // default ''
							flickityRef={(c) => (SliderNav.current = c)}
							options={flickityNav} // takes flickity options {}
							// reloadOnUpdate // default false
							// static // default false
						>
							{images.map((i, index) =>
								i.type ? (
									<Div
										onClick={handleThumbnailClick.bind(this, index)}
										className='thumbnail-image'
										key={index}
										bgImg={i.thumbnail}
										bgSize='cover'
										bgPos='center'
										shadow='2'
									/>
								) : (
									<Div
										onClick={handleThumbnailClick.bind(this, index)}
										className='thumbnail-image'
										key={index}
										bgImg={i.thumbnail}
										bgSize='cover'
										bgPos='center'
										shadow='3'
									/>
								),
							)}
						</Flickity>
					)}
				</>
			)}
		</Div>
	);
};
const CarouselSlider = ({content}) => {
	const SliderMain = useRef();
	return (
		<div className='carousel-gallery-wrapper2'>
			<Flickity
				className={'carousel-gallery2'} // default ''
				flickityRef={(c) => (SliderMain.current = c)}
				options={flickityMain_CarouselSlider} // takes flickity options {}
				// reloadOnUpdate // default false
				// static // default false
			>
				{content.map((Data, index) => (
					<Div key={index} h='100%'>{<Data />}</Div>
				))}
			</Flickity>
		</div>
	);
};

export { Slider, CarouselSlider };
