import { useState, useEffect } from 'react';
import { Div } from 'atomize';
import { useDispatch } from 'react-redux';
import { setPageMode,setLoading } from '../../Stores/pagestore';
import Head from 'next/head';
import { useRouter } from 'next/router';

import Product from '../../src/marketplace/product';
import Footer from '../../Components/footer';

export default function MarketPlacePage_Product() {
	const router = useRouter();
	const [pid, setPid] = useState(null);
	const dispatch = useDispatch();
	useEffect(() => {
		setPid(router.query?.pid);
	}, [router]);
	useEffect(() => {
		dispatch(setPageMode({ pageMode: '/marketplace/product' }));
		setTimeout(() => {
			dispatch(setLoading({ loading: false }));
		}, 1500);
		return () => {
			dispatch(setLoading({ loading: true }));
		};
	}, []);
	return (
		<>
			<Head>
				<title>{pid ? `Product - ${pid}` : 'Product'}</title>
				<meta
					name='viewport'
					content='initial-scale=1.0, width=device-width'
				/>
			</Head>
			<Div className='product-page'>
				<Product />
			</Div>
			<Footer />
		</>
	);
}
