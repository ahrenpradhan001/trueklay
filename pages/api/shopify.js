import axios from 'axios'
import Cors from 'cors';
import initMiddleware from '../../lib/init-middleware';

const cors = initMiddleware(
	// You can read more about the available options here: https://github.com/expressjs/cors#configuration-options
	Cors({
		// Only allow requests with GET, POST and OPTIONS
		methods: ['GET', 'POST', 'OPTIONS'],
	}),
);

export default async (req, res) => {
    await cors(req, res);
	const { method } = req;
    switch (method) {
        case 'GET':
            try {
                // const response = await API REQUEST
                const { action } = req.query;
                res.status(200).send({ success: false, data: 'no action specified' });
            } catch (error) {
                res.status(400).send({ success: false, data: 'nothing can be done' });
            }
            break;
        case 'POST':
            try {
                const { action } = req.body;
                res.status(200).send({ success: false, data: 'no action specified' });
			} catch (error) {
				res.status(400).send({ success: false, data: 'nothing can be done' });
			}
            break;
        default:
            res.status(400).send({success:false,data:'nothing can be done'})
            break;
    }
}