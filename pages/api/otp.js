import axios from 'axios';

const GenerateOtpLink = (phone_number) => (phone_number ? `https://2factor.in/API/V1/${process.env.OTP_API_KEY}/SMS/${phone_number}/AUTOGEN` : false);
const VerifyOtpLink = (session_id, otp_input) =>
	session_id && otp_input ? `https://2factor.in/API/V1/${process.env.OTP_API_KEY}/SMS/VERIFY/${session_id}/${otp_input}` : false;

const OTPAPI = async (req, res) => {
	const { method } = req;
	switch (method) {
		case 'GET':
			try {
				const { mode, phone_number, session_id, otp_input } = req.query;
				if (phone_number) {
					if (mode == 'generateOTP') {
						const response1 = await axios.get(GenerateOtpLink(phone_number));
						res.status(200).json({
							success: true,
							data: response1.data,
						});
					} else if (mode == 'verifyOTP') {
						if (session_id && otp_input) {
							try {
								const response2 = await axios.get(VerifyOtpLink(session_id, otp_input));
								res.status(200).json({
									success: true,
									data: response2.data,
								});
							} catch (err) {
								res.status(200).json({ success: false });
							}
						} else {
							res.status(201).json({
								success: false,
								data: 'missing session_id or otp_input',
							});
						}
					} else {
						res.status(201).json({
							success: false,
							data: 'invalid mode',
						});
					}
				} else {
					res.status(201).json({
						success: false,
						data: 'missing phone number',
					});
				}
			} catch (error) {
				res.status(400).json({ success: false });
			}
			break;
		case 'POST':
			res.status(200).json({
				success: false,
				data: 'nothing designed as of yet',
			});
			break;
		default:
			res.status(400).json({ success: false });
			break;
	}
};

export default OTPAPI;
