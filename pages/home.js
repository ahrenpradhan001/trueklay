// DOCUMENTATION: http://styletron.org

import { Div } from 'atomize';
import { useDispatch } from 'react-redux';
import { useEffect } from 'react';
import { setPageMode, setLoading } from '../Stores/pagestore';
import Head from 'next/head';

import Home from '../src/home';
import Footer from '../Components/footer';

export default function HomePage() {
	const dispatch = useDispatch();
	useEffect(() => {
		dispatch(setPageMode({ pageMode: '/home' }));
		setTimeout(() => {
			dispatch(setLoading({ loading: false }));
		}, 1500);
		return () => {
			dispatch(setLoading({ loading: true }));
		};
	}, []);
	return (
		<>
			<Head>
				<title>My page title</title>
				<meta
					name='viewport'
					content='initial-scale=1.0, width=device-width, maximum-scale=1.0, user-scalable=no'
				/>
			</Head>
			<Div className='home-page-image' style={{ zIndex: '-1' }} />
			<Div className='home-page'>
				<Home />
			</Div>
			<Footer />
		</>
	);
}