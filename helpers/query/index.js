export const CLIENTFITTINGFILTER = `query clientFittingFilter($clientID: ID!) {
  clientFittingFilter(
    filter: []
    sort: []
    search: ""
    clientID: $clientID
    contractorID: "f63ccee8-46d9-4ae7-8477-0bc1cbf25845"
    storeID: ""
    deleted: false
    start: 0
    limit: 10
    groupID: ""
    sex: ""
  ) {
    total
    items {
      product {
        id
        currency
        price
        name
        code
        groupID
        code
        sex
        available
      }
    }
  }
}
`;

export const CLIENT = `query client($clientID: ID!){
  client(id:$clientID){
    name
    email
    phone
    sex
  }
}
`;
