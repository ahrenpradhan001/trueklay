import React, { useState, useEffect } from 'react';
import { ThemeProvider, Icon, Button, Div, Text, Input } from 'atomize';
import { useSelector } from 'react-redux';

import {Slider} from '../../../Components/common/slider';
import { SliderButton } from '../../../Components/common/custombutton';
// import ImageGallery from 'react-image-gallery';
// import Flickity from 'react-flickity-component';

const images = [
	{
		original:
			'https://assets.myntassets.com/dpr_2,q_60,w_210,c_limit,fl_progressive/assets/images/productimage/2020/8/18/fa94ddb5-246b-4209-999b-ecef74d0a9e01597702012954-1.jpg',
		thumbnail:
			'https://assets.myntassets.com/dpr_2,q_60,w_210,c_limit,fl_progressive/assets/images/productimage/2020/8/18/fa94ddb5-246b-4209-999b-ecef74d0a9e01597702012954-1.jpg',
	},
	{
		original:
			'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/productimage/2020/8/18/a1360033-6080-4956-a194-c5dce0ae17be1597702013002-2.jpg',
		thumbnail:
			'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/productimage/2020/8/18/a1360033-6080-4956-a194-c5dce0ae17be1597702013002-2.jpg',
	},
	{
		original:
			'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/productimage/2020/8/18/84b30cb0-f2fc-42be-82fe-de60d477d57b1597702013048-3.jpg',
		thumbnail:
			'https://assets.myntassets.com/h_720,q_90,w_540/v1/assets/images/productimage/2020/8/18/84b30cb0-f2fc-42be-82fe-de60d477d57b1597702013048-3.jpg',
	},
	{
		original: 'https://picsum.photos/id/1019/800/400/',
		thumbnail: 'https://picsum.photos/id/1019/200/100/',
	},
	{
		type: 'video',
		video: 'https://media.w3.org/2010/05/sintel/trailer_hd.mp4',
		thumbnail:
			'https://i.ytimg.com/vi/k7zqno4RWIo/hq720.jpg?sqp=-oaymwEcCNAFEJQDSFXyq4qpAw4IARUAAIhCGAFwAcABBg==&rs=AOn4CLCM7-BnohUYSBc2f5vxV36W-bqiGw',
	},
];

const theme = {
	grid: {
		colCount: 12,
		gutterWidth: '20px',
	},
	colors: {
		brand: '#1a89f0',
		brandHover: '#ffc5c5',
	},
	shadows: {
		'card-shadow': '-2px 2px 8px 8px rgba(0, 0, 0, 0.04)',
	},
};

const ProductDesciption = () => {
	return (
		<>
			<Div>
				<Text textWeight='bold' textSize='18px' m={{ b: '4px' }}>
					PRODUCT DETAILS
				</Text>
				<Text>
					A pair of round-toe grey sneakers, has regular styling,
					lace-up detail Synthetic upper Cushioned footbed Textured
					and patterned outsole Warranty: 7 days Warranty provided by
					brand/manufacturer
				</Text>
			</Div>
			<Div>
				<Text
					textWeight='bold'
					textSize='16px'
					m={{ t: '1em', b: '0.5em' }}>
					Material & Care
				</Text>
				<Text>Synthetic</Text>
				<Text>Wipe with a clean, dry cloth to remove dust</Text>
			</Div>
		</>
	);
};

const ProductSpecification = ({ specification }) => {
	return (
		<>
			<Text textWeight='bold' textSize='18px'>
				SPECIFICATION
			</Text>
			<Div
				w='100%'
				d='flex'
				justify='space-between'
				style={{
					flexWrap: 'wrap',
				}}>
				{specification.map((spec, index) => {
					return (
						<>
							<Div
								key={index}
								w='50%'
								minH='3em'
								p='0.5em'
								d='flex'
								flexDir='column'
								justify='space-between'>
								<Text>{spec.name}</Text>
								<Text textWeight='bold'>{spec.content}</Text>
							</Div>
							{index % 2 == 1 && (
								<Div className='horizontal-separator' />
							)}
						</>
					);
				})}
			</Div>
		</>
	);
};

export default function Product() {
	const pageStore = useSelector((state) => state.pageStore);
	const [mode, setMode] = useState(0);
	// useEffect(() => { })
	const specification = [
		{
			name: 'Type',
			content: 'Sneakers',
		},
		{
			name: 'Pattern',
			content: 'Solid',
		},
		{
			name: 'Insole',
			content: 'Comfort Insole',
		},
		{
			name: 'Shoe Width',
			content: 'Regular',
		},
		{
			name: 'Fastening',
			content: 'Lace-Ups',
		},
		{
			name: 'Sole Material',
			content: 'PU',
		},
		{
			name: 'Fastening',
			content: 'Lace-Ups',
		},
		{
			name: 'Sole Material',
			content: 'PU',
		},
	];
	return (
		<ThemeProvider theme={theme}>
			<Div className='product-container'>
				<Div className='product-desktop-view'>
					<Div className='marketplace-section product-row-1'>
						<Div d='flex' align='center' w='fit-content' h='60px'>
							<Button
								rounded='circle'
								border='1px solid'
								borderColor='brand'
								bg='transparent'
								w='fit-content'
								h='fit-content'
								m='0px'
								p='2px'>
								<Icon
									name='LeftArrow'
									size='20px'
									color='brand'
								/>
							</Button>
							<Text
								textSize='16px'
								m={{ l: '1em' }}
								w='max-content'>
								Back to results
							</Text>
						</Div>
					</Div>
					<Div className='marketplace-section product-row-2'>
						<Div className='product-col-1'>
							<Div>
								<Slider images={images} />
							</Div>
							<Div className='product-inner-row'>
								<Div className='product-inner-col'>
									<ProductDesciption />
								</Div>
								<Div className='product-inner-col'>
									<ProductSpecification
										specification={specification}
									/>
								</Div>
							</Div>
						</Div>
						<Div className='product-col-2'>
							<Div className='product-col-section'>
								<Div className='product-col-content'>
									<SliderButton
										mode={mode}
										setMode={setMode.bind(
											this,
											(mode + 1) % 2,
										)}
									/>
								</Div>
								<Div className='product-col-content product-main-description'>
									<Div d='flex' justify='space-between'>
										<Text className='product-description-title'>
											COHEN HAYES
										</Text>
										<Div d='flex' m={{ r: '2em' }}>
											<Text>4.8</Text>
											<Icon
												name='StarSolid'
												size='18px'
												w='14px'
												color='warning600'
											/>
										</Div>
									</Div>
									<Div className='product-col-content'>
										<Text className='product-description-description'>
											Men gray sneakers
										</Text>
										<Text className='product-description-id'>
											SKU 1287639218
										</Text>
									</Div>
									<Div className='product-col-content'>
										<Div className='product-description-price'>
											<Text
												className='product-description-new-price'
												m={{ r: '2em' }}
												textSize='26px'>
												Rs 2499
											</Text>
											<Text className='product-description-old-price'>
												Rs 2499
											</Text>
										</Div>
									</Div>
									<Div className='product-col-content product-description-color'>
										<Text>Color</Text>
										<Div className='product-description-color-options'>
											{[
												`#fff`,
												'#aaa',
												'green',
												'blue',
												'red',
												'black',
											].map((c) => (
												<Div
													// m={{ r: '0.5em' }}
													// rounded='circle'
													// h='2em'
													// w='2em'
													bg={c}
													// border='1px solid'
													// borderColor='#3e3e3e'
												/>
											))}
										</Div>
									</Div>
									<Div className='product-col-content'>
										<Div
											border='1px solid'
											borderColor='#313131'
											p='10px'
											m='10px'
											rounded='2em'
											d='flex'
											flexDir='column'>
											<Div
												minH='40px'
												w='100%'
												bg='#e3e3e3'
												m={{ b: '10px' }}
											/>
											<Button
												h='50px'
												textColor='#fff'
												bg='brand'
												rounded='2em'>
												ADD TO BAG
											</Button>
										</Div>
									</Div>
								</Div>
							</Div>
						</Div>
					</Div>
				</Div>
				<Div className='product-mobile-view'>
					<Div className='product-mobile-view-page-wrapper'>
						<Div className='marketplace-section'>
							<Slider images={images} mobileMode={true} />
						</Div>
						<Div className='marketplace-section product-main-description'>
							<Div className='product-description-row-1'>
								<Text className='product-description-title'>
									COHEN
								</Text>
								<Icon name='Heart' size='24px' />
							</Div>
							<Text className='product-description-description'>
								MEN GRAY SNEAKERS
							</Text>
							<Text className='product-description-id'>
								SKU Code: 123456789
							</Text>
							<Div flexGrow='1' />
							<Text className='product-description-discount'>
								(50% OFF)
							</Text>
							<Div className='product-description-price'>
								<Div d='flex' align='flex-end'>
									<Text className='product-description-new-price'>
										Rs 2499
									</Text>
									<Text className='product-description-old-price'>
										Rs 4999
									</Text>
								</Div>
								<Div d='flex'>
									<Text>4.8</Text>
									<Icon
										name='StarSolid'
										size='18px'
										w='14px'
										color='warning600'
									/>
								</Div>
							</Div>
						</Div>
					</Div>
					<Div className='profile-mobile-content'>
						<Div className='product-mobile-page-divider' />
						<Div className='marketplace-section product-description-color'>
							<Text>COLOR</Text>
							<Div className='product-description-color-options'>
								{[
									`#fff`,
									'#aaa',
									'green',
									'blue',
									'red',
									'black',
								].map((c) => (
									<Div
										// m={{ r: '0.5em' }}
										// rounded='circle'
										// h='2em'
										// w='2em'
										bg={c}
										// border='1px solid'
										// borderColor='#3e3e3e'
									/>
								))}
							</Div>
						</Div>
						<Div className='product-mobile-page-divider' />
						<Div className='marketplace-section'>
							<Text textSize='18px' textWeight='bold'>
								DELIVERY OPTIONS
							</Text>
							<Input
								m={{ t: '1em' }}
								placeholder='Enter Pincode'
								rounded='circle'
								suffix={
									<Button
										pos='absolute'
										onClick={() => console.log('clicked')}
										bg='transparent'
										top='0'
										right='0'
										rounded='circle'
										textColor='brand'>
										CHECK
									</Button>
								}
							/>
						</Div>
						<Div className='product-mobile-page-divider' />
						<Div className='marketplace-section'>
							<ProductDesciption />
						</Div>
						<Div className='marketplace-section'>
							<ProductSpecification
								specification={specification}
							/>
						</Div>
					</Div>
					<Div className='product-mobile-page-divider' />
					<Div className='marketplace-section'>
						<Div
							className={
								pageStore.footerInView
									? 'marketplace-section floating-nav-bar'
									: 'marketplace-section floating-nav-bar floating-nav-bar-fixed'
							}>
							<Button
								w='100%'
								h='50px'
								textSize='18px'
								bg='brand'>
								ADD TO BAG
							</Button>
						</Div>
						<Div
							className={
								pageStore.footerInView
									? 'floating-nav-bar-overlay floating-nav-bar-overlay-visible'
									: 'floating-nav-bar-overlay floating-nav-bar-overlay-not-visible'
							}>
							<Div className='product-mobile-page-divider' />
						</Div>
					</Div>
				</Div>
			</Div>
		</ThemeProvider>
	);
}
