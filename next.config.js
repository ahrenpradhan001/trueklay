const path = require('path');
// const withCSS = require('@zeit/next-css');

module.exports = {
	async headers() {
		return [
			{
				// matching all API routes
				source: '/api/:path*',
				headers: [
					{ key: 'Access-Control-Allow-Credentials', value: 'true' },
					{ key: 'Access-Control-Allow-Origin', value: '*' },
					{ key: 'Access-Control-Allow-Methods', value: 'GET,OPTIONS,PATCH,DELETE,POST,PUT' },
					{
						key: 'Access-Control-Allow-Headers',
						value:
							'X-CSRF-Token, X-Requested-With, Accept, Accept-Version, Content-Length, Content-MD5, Content-Type, Date, X-Api-Version',
					},
				],
			},
		];
	},
	sassOptions: {
		includePaths: [path.join(__dirname, 'styles')],
	},
	webpack: function (config) {
		config.externals = config.externals || {};
		config.externals['styletron-server'] = 'styletron-server';
		return config;
	},
};

// module.exports = withCSS();
