import { createSlice } from '@reduxjs/toolkit';

const pageSlice = createSlice({
	name: 'page',
	initialState: {
		logoButton: null,
		pageMode: '/',
		pageOnTop: true,
		footerInView: false,
		loading: true,
	},
	reducers: {
		loadLogoButton: (state, action) => {
			const { loadLogoButton } = action.payload;
			state.logoButton = loadLogoButton;
		},
		setPageMode: (state, action) => {
			const { pageMode } = action.payload;
			state.pageMode = pageMode;
		},
		setPageOnTop: (state, action) => {
			const { pageOnTop } = action.payload;
			state.pageOnTop = pageOnTop;
		},
		setFooterInView: (state, action) => {
			const { footerInView } = action.payload;
			state.footerInView = footerInView;
		},
		setLoading: (state, action) => {
			const { loading } = action.payload;
			state.loading = loading;
		},
	},
});

export const {
	loadLogoButton,
	setPageMode,
	setPageOnTop,
	setFooterInView,
	setLoading,
} = pageSlice.actions;
export default pageSlice.reducer;
