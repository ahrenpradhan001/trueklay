import { createSlice } from '@reduxjs/toolkit';
import { getClientID, getClientDetails, getClientFittingFilterDetails } from '../helpers/apicalls';

const teknofeetSlice = createSlice({
	name: 'teknofeet',
	initialState: {
		client: {
			clientID: null,
			name: null,
			phone_number: null,
		},
		clientFittingFilter: {
			items: null,
			total: null,
		},
		fitmap: null,
	},
	reducers: {
		updateTeknofeetStoreParam: (state, action) => {
			const { actionType, value } = action.payload;
			switch (actionType) {
				case 'clientID':
					state.client = {
						...state.client,
						clientID: value,
					};
					break;
				case 'clientDetails':
					state.client = {
						...state.client,
						...value,
					};
					break;
				case 'clientFittingFilter':
					state.clientFittingFilter = {
						...state.clientFittingFilter,
						...value,
					};
					break;
				default:
					console.log('no parameter provided');
					break;
			}
		},
	},
});

// Asynchronous thunk action
export function setTeknofeetClientID(data) {
	return async (dispatch) => {
		try {
			const { email, phone_number } = data;
			const response = await getClientID({
				email,
				phone_number,
			});
			if (response.success) {
				const { clientID } = response.data;
				dispatch(
					updateTeknofeetStoreParam({
						actionType: 'clientID',
						value: clientID,
					}),
				);
				const response2 = await getClientDetails(clientID);
				if (response2.success) {
					console.log(response2.data);
					dispatch(
						updateTeknofeetStoreParam({
							actionType: 'clientDetails',
							value: response2.data.client,
						}),
					);
					const response3 = await getClientFittingFilterDetails(clientID);
					if (response3.success) {
						const { clientFittingFilter } = response3.data;
						dispatch(
							updateTeknofeetStoreParam({
								actionType: 'clientFittingFilter',
								value: clientFittingFilter,
							}),
						);
					}
				}
			}
		} catch (error) {
			console.log(error);
		}
	};
}

export const { updateTeknofeetStoreParam } = teknofeetSlice.actions;

export default teknofeetSlice.reducer;
