import { createAsyncThunk, createSlice } from '@reduxjs/toolkit';

// aync example
const fetchUserById = createAsyncThunk('users/fetchByIdStatus', async (userId, thunkAPI) => {
	const response = await userAPI.fetchById(userId);
	return response.data;
});

const getFilteredProducts = ({ productList, wishList, filters, pagination }) =>
	productList
		.map((product) => ({
			...product,
			addedToWishlist: wishList.includes(product.id),
		}));
		// .slice(pagination.index * pagination.maxContentPerPage, (pagination.index + 1) * pagination.maxContentPerPage);

const shopifySlice = createSlice({
	name: 'shopify',
	initialState: {
		customer: 'ahren',
		client: {
			keepMarketplaceSplashScreen: true,
		},
		pagination: {
			index: 0,
			maxContentPerPage: 16,
		},
		isCartOpen: false,
		isFilterOpen: false,
		allProducts: [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28].map((i) => ({
			id: i,
			name: `COHEN ${i}`,
			description: 'Men Gray sneakers',
			SKU_code: '123456789',
			images: [
				{
					src:
						'https://assets.myntassets.com/dpr_2,q_60,w_210,c_limit,fl_progressive/assets/images/productimage/2020/8/18/fa94ddb5-246b-4209-999b-ecef74d0a9e01597702012954-1.jpg',
				},
			],
			price: {
				old: '4999',
				new: '2499',
				discount: '50%',
			},
			qty: 10,
			rating: '4.8',
		})),
		currentProducts: [],
		filteredProducts: [],
		filters: [
			{
				title: 'fit-type',
				options: [
					{ name: 'best fit', qty: 100, status: false },
					{ name: 'good fit', qty: 44, status: false },
					{ name: 'average fit', qty: 4, status: false },
				],
			},
			{
				title: 'brand',
				options: [
					{ name: 'best fit', qty: 10, status: false },
					{ name: 'good fit', qty: 10, status: false },
					{ name: 'average fit', qty: 10, status: false },
				],
			},
			{
				title: 'categories',
				options: [
					{ name: 'best fit', qty: 10, status: false },
					{ name: 'good fit', qty: 10, status: false },
					{ name: 'average fit', qty: 10, status: false },
				],
			},
			{
				title: 'color',
				options: [
					{ name: 'best fit', qty: 10, status: false },
					{ name: 'good fit', qty: 10, status: false },
					{ name: 'average fit', qty: 10, status: false },
				],
			},
		],
		currentRemovableProduct: null,
		currentRemovableWishProduct: null,
		cart: [],
		cartDetails: {
			totalPrice: 0,
		},
		wishlist: [2],
	},
	reducers: {
		loadShopifyStore: (state, action) => {
			state.filteredProducts = getFilteredProducts({
				productList: state.allProducts,
				wishList: state.wishlist,
				filters: state.filters,
				pagination: state.pagination,
			});
		},
		toggleCart: (state, action) => {
			state.isFilterOpen = false;
			state.isCartOpen = !state.isCartOpen;
		},
		toggleFilter: (state, action) => {
			state.isCartOpen = false;
			state.isFilterOpen = !state.isFilterOpen;
		},
		toggleFilterValue: (state, action) => {
			const { title, name } = action.payload;
			state.filters = [
				...state.filters.map((f) =>
					f.title == title
						? {
								...f,
								options: f.options.map((option) =>
									option.name == name
										? {
												...option,
												status: !option.status,
										  }
										: option,
								),
						  }
						: f,
				),
			];
			state.filteredProducts = getFilteredProducts({
				productList: state.allProducts,
				wishList: state.wishlist,
				filters: state.filters,
				pagination: state.pagination,
			});
		},
		handleProduct: (state, action) => {
			const { id, actionType } = action.payload;
			switch (actionType) {
				case 'ADD_TO_WISHLIST':
					if (state.wishlist.indexOf(id) < 0) {
						state.wishlist = [...state.wishlist, id];
					}
					break;
				case 'REMOVE_FROM_WISHLIST':
					state.wishlist = state.wishlist.filter((w) => w != id);
					// if (state.currentRemovableWishProduct) {
					// 	state.wishlist = state.wishlist.filter(
					// 		(w) => w != state.currentRemovableWishProduct.id,
					// 	);
					// 	state.currentRemovableWishProduct = null;
					// } else {
					// 	state.currentRemovableWishProduct = { id };
					// }
					break;
				case 'ABORT_REMOVE_FROM_WISHLIST':
					state.currentRemovableWishProduct = null;
					break;
				case 'ADD':
					state.cart = state.cart.map((p) =>
						p.id == id
							? {
									...p,
									qty: p.qty + 1,
							  }
							: p,
					);
					break;
				case 'SUBTRACT':
					state.cart = state.cart.map((p) => {
						if (p.id == id) {
							if (p.qty - 1 == 0) {
								state.currentRemovableProduct = p;
							} else {
								return {
									...p,
									qty: p.qty - 1,
								};
							}
						}
						return p;
					});
					// .filter((f) => f.qty > 0);
					break;
				case 'SELECT_REMOVABLE_PRODUCT':
					state.cart = state.cart.map((p) => {
						if (p.id == id) {
							state.currentRemovableProduct = p;
						}
						return p;
					});
					break;
				case 'REMOVE':
					state.cart = state.cart.filter((p) => p.id != id);
					if (id == state.currentRemovableProduct.id) {
						state.currentRemovableProduct = null;
					}
					state.isCartOpen = true;
					break;
				case 'ABORT_REMOVE':
					state.currentRemovableProduct = null;
					state.isCartOpen = true;
					break;
				default:
					alert('action not specified');
					break;
			}
			state.filteredProducts = getFilteredProducts({
				productList: state.allProducts,
				wishList: state.wishlist,
				filters: state.filters,
				pagination: state.pagination,
			});
		},
		addToCart: (state, action) => {
			const { id, cartStat, added } = action.payload;
			state.cart =
				state.cart.filter((d) => d.id == id).length > 0
					? state.cart.map((e) => (e.id == id ? { ...e, qty: added ? e.qty : e.qty + 1 } : e))
					: [
							...state.cart,
							{
								id,
								qty: 1,
							},
					  ];
			state.isCartOpen = typeof cartStat != undefined ? cartStat || !!added : true;
			state.filteredProducts = getFilteredProducts({
				productList: state.allProducts,
				wishList: state.wishlist,
				filters: state.filters,
				pagination: state.pagination,
			});
		},
		handleMarketplaceSplashScreen: (state, action) => {
			const { actionType } = action.payload;
			switch (actionType) {
				case 'CLOSE':
					state.client = {
						...state.client,
						keepMarketplaceSplashScreen: false,
					};
					break;
				default:
					console.log('action type not defined');
			}
		},
		handlePagination: (state, action) => {
			const { actionType, index } = action.payload;
			switch (actionType) {
				case 'CHANGEPAGE':
					state.pagination = {
						...state.pagination,
						index,
					};
					state.filteredProducts = getFilteredProducts({
						productList: state.allProducts,
						wishList: state.wishlist,
						filters: state.filters,
						pagination: state.pagination,
					});
					break;
				default:
					console.log('action type not defined');
					break;
			}
		},
	},
	extraReducers: {
		// Add reducers for additional action types here, and handle loading state as needed
		[fetchUserById.fulfilled]: (state, action) => {
			// Add user to the state array
			state.entities.push(action.payload);
		},
	},
});

export function setShopifyStore(data) {
	return true;
}

export const {
	loadShopifyStore,
	toggleCart,
	toggleFilter,
	toggleFilterValue,
	addToCart,
	handleProduct,
	handleMarketplaceSplashScreen,
	handlePagination,
} = shopifySlice.actions;
export { fetchUserById };
export default shopifySlice.reducer;
