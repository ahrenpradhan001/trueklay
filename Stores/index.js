import { configureStore, getDefaultMiddleware } from '@reduxjs/toolkit';
import { combineReducers } from 'redux';

import shopifyReducer from './shopifystore';
import pageReducer from './pagestore';
import teknofeetReducer from './teknofeetstore';

const rootReducer = combineReducers({
	shopifyStore: shopifyReducer,
	pageStore: pageReducer,
	teknofeetStore: teknofeetReducer,
});

export default configureStore({
	reducer: rootReducer,
	middleware: (getDefaultMiddleware) =>
		getDefaultMiddleware({
			serializableCheck: false,
		}),
});
