import { createSlice } from '@reduxjs/toolkit';

const apolloSlice = createSlice({
	name: 'apollo',
	initialState: {
		apolloClient: null,
	},
	reducers: {
		setApolloClient: (state, action) => {
			const { apolloClient } = action.payload;
			state.apolloClient = apolloClient;
		},
	},
});

export const { setApolloClient } = apolloSlice.actions;
export default apolloSlice.reducer;
